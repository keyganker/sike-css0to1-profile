-   比reset.css更好的选择：[normalize.css](http://jerryzou.com/posts/aboutNormalizeCss/)

-   前段开发工具[browsersync](http://www.browsersync.io/)

-   有固定宽度的块级元素，可以使用以下方式来水平居中
    margin:0 auto;

-   行级元素水平居中可以在父级元素设置
     text-align: center;

-   在设置margin的时候，父容器和子元素之间的margin-top和margin-bottom会发生重合[Collapsing Margins](http://www.sitepoint.com/web-foundations/collapsing-margins/)，要想消除重合，可以在父容器上设置
padding：1px;

-   css属性排列顺序 [CSS property order](http://markdotto.com/2011/11/29/css-property-order/)

    - 定位属性: position, float, z-index, clear
    - 盒模型相关属性: padding, margin, display, width, height, border
    - 字体相关CSS2: 视觉相关属性 (background)
    - CSS3 属性: (border-radius, box-shadow)